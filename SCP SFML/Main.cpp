/*

#include <SFML/Graphics.hpp>
#include <iostream>
int main()
{

// Create a new render-window
sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
// Create a new render-texture
sf::RenderTexture texture;
if (!texture.create(500, 500))
return -1;
sf::CircleShape shape(100.f);
shape.setFillColor(sf::Color::Magenta);
texture.clear(sf::Color::Transparent);
texture.draw(shape);
// The main loop
while (window.isOpen())
{
	// Event processing
	// ...
	// Clear the whole texture with red color

	// Draw stuff to the texture


	//window.setVerticalSyncEnabled(1);
	window.setFramerateLimit(60); // FrameRate Capped at 60

   // shape is a sf::Shape

	// We're done drawing to the texture
	texture.display();
	// Now we start rendering to the window, clear it first
	window.clear();
	// Draw the texture
	sf::Sprite sprite(texture.getTexture());
	window.draw(sprite);
	// End the current frame and display its contents on screen
	window.display();
}
}
*/

/*
void populateShapeVector(sf::RenderWindow& window, std::vector<sf::CircleShape> &shapes, const std::vector<DrawableNode> &screenNodeVec)
{
	srand(time(NULL));
	for (DrawableNode nodeToDraw : screenNodeVec)
	{
	sf::CircleShape shape(30.f);
	shape.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255));
	shape.setPosition(nodeToDraw.x, nodeToDraw.y);
	shapes.push_back(shape);
	}
}

	//populateShapeVector(window, shapes, screenNodeVec);

	/*
	sf::CircleShape shape(10.f);
	shape.setPosition(960, 540);
	shape.setFillColor(sf::Color::Green);
	shapes.push_back(shape);

		std::vector<sf::CircleShape> shapes;

	//for (sf::CircleShape currShape : shapes)
	//{
	//	window.draw(currShape);
	//}

*/

/*
sf::CircleShape testShape;
testShape.setRadius(0.5);
testShape.setFillColor(sf::Color::Magenta);
testShape.setPosition({ -555, 400 });

sf::CircleShape testShape2;
testShape2.setRadius(0.5);
testShape2.setFillColor(sf::Color::Cyan);
testShape2.setPosition({ -554, 400 });

sf::CircleShape testShape3;
testShape3.setRadius(0.5);
testShape3.setFillColor(sf::Color::White);
testShape3.setPosition({ -555, 401 });

sf::CircleShape testShape4;
testShape4.setRadius(0.5);
testShape4.setFillColor(sf::Color::Yellow);
testShape4.setPosition({ -554, 401 });

sf::VertexArray testPoint(sf::Points, 4);
testPoint[0].position = { -555,400 };
testPoint[1].position = { -554,400 };
testPoint[2].position = { -555,401 };
testPoint[3].position = { -554,401 };

		window.draw(testShape);
		window.draw(testShape2);
		window.draw(testShape3);
		window.draw(testShape4);
		window.draw(testPoint);

*/

/*
			window.draw(greenPixelSprite);
				sf::Texture greenPixeltex;
	if (!greenPixeltex.loadFromFile("green_pixel.png"))
	{
		return -1;
	}
	sf::Sprite greenPixelSprite(greenPixeltex);
	greenPixelSprite.setPosition(line[0].position.x - 0.5, line[0].position.y - 0.5);
*/

#define MIN_LAT 573929
#define MIN_LONG 4945029
#define MAX_LAT 652685
#define MAX_LONG 5018275
#define M_PI 3.14


#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream> 
#include <vector>
#include <algorithm> 
#include <unordered_map>
#include <iterator>

struct table
{
	int path;
	int distance;
	bool known;
};

class Muchie {
public:
	int neighbor;
	int cost;
	Muchie *next;

	Muchie(int neighbor, Muchie *next, int cost) 
	{
		this->neighbor = neighbor;
		this->next = next;
		this->cost = cost;
	}
};

struct Edge
{
	int src;
	int dest;
	int cost;
};

struct Node
{
	int id;
	int latitude;
	int longitutde;
};

struct DrawableNode
{
	int id;
	int x;
	int y;
};

int minUnknownVertex(std::vector<table> T)
{
	int i;
	int minVertex = -1;
	int minDistance = INT_MAX;
	for (i = 0; i < T.size(); i++)
	{
		if ((!T[i].known) && (T[i].distance < minDistance))
		{
			minVertex = i;
			minDistance = T[i].distance;
		}
	}
	return minVertex;
}

void Dijkstra(std::vector<Muchie*> Graf, int sursa, std::vector<table> &tabel) 
{
	int i, v;
	Muchie* e;
	table temp;
	temp.distance = INT_MAX;
	temp.known = false;
	temp.path = -1;
	for (i = 0; i < Graf.size(); i++)
	{
		tabel.push_back(temp);
	}
	tabel[sursa].distance = 0;
	for (i = 0; i < Graf.size(); i++)
	{
		v = minUnknownVertex(tabel);
		tabel[v].known = true;

		for (e = Graf[v]; e != NULL; e = e->next)
		{
			if (tabel[e->neighbor].distance > tabel[v].distance + e->cost)
			{
				tabel[e->neighbor].distance = tabel[v].distance + e->cost;
				tabel[e->neighbor].path = v;
			}
		}
	}
}

void populateShapeVector(sf::RenderWindow& window, std::unordered_map<int, sf::CircleShape> &shapes, const std::vector<DrawableNode> &screenNodeVec)
{
	srand(time(NULL));
	for (DrawableNode nodeToDraw : screenNodeVec)
	{
		sf::CircleShape shape(0.5);
		shape.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255));
		shape.setPosition(nodeToDraw.x, nodeToDraw.y);
		//shape.setOrigin(shape.getRadius() - 1, shape.getRadius() + 1);
		shapes.insert({ nodeToDraw.id, shape });
	}
}

DrawableNode getNodeCoordinates(long int longitude, long int latitude)
{
	DrawableNode node;
	//long double xScale = 1920.f / (MAX_LONG - MIN_LONG);
	//long double yScale = 1080.f / (MAX_LAT - MIN_LAT);
	long double xScale = 1360.f / (MAX_LONG - MIN_LONG);
	long double yScale = 768.f / (MAX_LAT - MIN_LAT);

	long double xLong = (long double)(longitude - MIN_LONG) * xScale;
	long double yLat = (long double)(latitude - MIN_LAT) * yScale;

	node.x = xLong;
	node.y = yLat;
	//
	return node;
}

void readNodes(std::vector<Node> &nodeVec, std::vector<DrawableNode> &screenNodeVec)
{
	std::ifstream file;
	file.open("node.txt");
	Node temp;
	DrawableNode d_temp;
	std::pair<int, int> coordinates;
	while (!file.eof())
	{
		file >> temp.id >> temp.latitude >> temp.longitutde;
		nodeVec.push_back(temp);
		d_temp = getNodeCoordinates(temp.longitutde, temp.latitude);
		d_temp.id = temp.id;
		screenNodeVec.push_back(d_temp);
	}
	std::cout << "Vec size: " << nodeVec.size() << std::endl;
	std::cout << "dVec size: " << screenNodeVec.size() << std::endl;
	file.close();
}

void readEdges(std::vector<Edge> &screenEdgeVec)
{
	std::ifstream file;
	file.open("edge.txt");
	Edge temp;
	while (!file.eof())
	{
		file >> temp.src >> temp.cost >> temp.dest;
		screenEdgeVec.push_back(temp);
	}
	std::cout << "Edge Vec size: " << screenEdgeVec.size() << std::endl;
	file.close();
}

void showMinMax(const std::vector<Node> &nodeVec)
{
	std::vector<int> lats;
	std::vector<int> longs;
	for (const Node &node : nodeVec)
	{
		lats.push_back(node.latitude);
		longs.push_back(node.longitutde);
	}
	std::cout << "Min latitude: " << *(std::min_element(std::begin(lats), std::end(lats))) << std::endl;
	std::cout << "Min longitude: " << *(std::min_element(std::begin(longs), std::end(longs))) << std::endl;
	std::cout << "Max latitude: " << *(std::max_element(std::begin(lats), std::end(lats))) << std::endl;
	std::cout << "Max longitude: " << *(std::max_element(std::begin(longs), std::end(longs))) << std::endl;
}

sf::VertexArray pointsArray(const std::vector<DrawableNode> &screenNodeVec)
{
	sf::VertexArray points(sf::Points, screenNodeVec.size());
	for (int i = 0; i < screenNodeVec.size(); i++)
	{
		points[i].position = sf::Vector2f(screenNodeVec[i].x + 0.5, screenNodeVec[i].y + 0.5);
		points[i].color = sf::Color::Red;
	}
	return points;
}

void zoomViewAt(sf::Vector2i pixel, sf::RenderWindow& window, float zoom)
{
	const sf::Vector2f beforeCoord{ window.mapPixelToCoords(pixel) };
	sf::View view{ window.getView() };
	view.zoom(zoom);
	window.setView(view);
	const sf::Vector2f afterCoord{ window.mapPixelToCoords(pixel) };
	const sf::Vector2f offsetCoords{ beforeCoord - afterCoord };
	view.move(offsetCoords);
	window.setView(view);
}


int main()
{
	std::vector<Muchie*> muchii;
	std::vector<table> tabel;

	sf::VertexArray line(sf::Lines, 2);
	int numberOfClicks = 0;
	line[0].color = sf::Color::Cyan;
	line[1].color = sf::Color::Cyan;

	sf::Vector2f realPixel;
	sf::RenderTexture tex;

	std::vector<Node> nodeVec;
	std::vector<DrawableNode> screenNodeVec;

	readNodes(nodeVec,screenNodeVec);
	std::vector<Edge> screenEdgeVec;
	readEdges(screenEdgeVec);

	for (int i = 0; i < screenEdgeVec.size(); i++)
	{
		muchii.push_back(NULL);
	}

	for (Edge edge : screenEdgeVec)
	{
		Muchie* temp = new Muchie(edge.dest, muchii[edge.src], edge.cost);
		muchii[edge.src] = temp;
	}

	//Dijkstra(muchii, 0, tabel);

	//	for (int i = 0; i < 50; i++)
	//{
	//	std::cout << "T[Path]= " << tabel[i].path << "T[Distance]= " << tabel[i].distance;
	//}

	showMinMax(nodeVec);
	sf::VertexArray points = pointsArray(screenNodeVec);


	const float zoomAmount{ 1.1f };

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

//	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Algoritmica Graficii", sf::Style::Default, settings);
	sf::RenderWindow window(sf::VideoMode(1360, 768), "Algoritmica Graficii", sf::Style::Fullscreen, settings);

	std::unordered_map<int, sf::CircleShape> newPoints;
	populateShapeVector(window, newPoints, screenNodeVec);

	window.setVerticalSyncEnabled(1);
	window.setFramerateLimit(60); // FrameRate Capped at 60

	sf::View view(window.getDefaultView());


	sf::Clock clock;
	window.clear();
	//sf::Texture texture;
	//if (!texture.loadFromFile("Luxemburg.png"))
	//{
	//	system("pause");
	//	std::cout << "WTF";
	//	return -1;
	//}
//	sf::Sprite background(texture);
	
	std::vector<table> testTable;
	std::vector<Muchie*> testMuchie;
	std::vector<Edge> testDia = { {0,1,10}, {0,3,3}, {1,2,11}, {3,2,6} };
	for (int i = 0; i < testDia.size(); i++)
	{
		testMuchie.push_back(NULL);
	}

	for (Edge edge : testDia)
	{
		Muchie* temp = new Muchie(edge.dest, testMuchie[edge.src], edge.cost);
		testMuchie[edge.src] = temp;
	}
	int nodStart = 0;
	Dijkstra(testMuchie, 0, testTable);
	for (int i = 0; i < testTable.size(); ++i)
	{
		std::cout << "SRC: " << nodStart << " Destinatie: " << i << " Distanta: " << testTable[i].distance << " Path: " << i;
		int k = i;
		while (testTable[k].path != -1)
		{
			std::cout << " -> " << testTable[k].path;
			k = testTable[k].path;
		}
		std::cout << std::endl;
	}
	system("pause");

	
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed || event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
				window.close();
			else if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::BackSpace)
				{
					view = window.getDefaultView();
					window.setView(view);
				}

				else if (event.key.code == sf::Keyboard::Z)
				{
					//zoomViewAt({ sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y }, window, (1.f / zoomAmount));
					numberOfClicks = 0;
					std::cout << "Mouse x: " << sf::Mouse::getPosition(window).x << ", \nMouse y:" << sf::Mouse::getPosition(window).y << std::endl;
				}

				else if (event.key.code == sf::Keyboard::X)
				{
					zoomViewAt({ sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y }, window, zoomAmount);
					std::cout << "Mouse x: " << sf::Mouse::getPosition(window).x << ", \nMouse y:" << sf::Mouse::getPosition(window).y << std::endl;
				}
			}
			
			else if (event.type == sf::Event::MouseWheelScrolled)
			{
				if (event.mouseWheelScroll.delta > 0)
				{
					zoomViewAt({ event.mouseWheelScroll.x, event.mouseWheelScroll.y }, window, (1.f / zoomAmount));
					std::cout << "Mouse x: " << event.mouseWheelScroll.x << ", \nMouse y:" << event.mouseWheelScroll.y << std::endl;

				}
				else if (event.mouseWheelScroll.delta < 0)
				{
					zoomViewAt({ event.mouseWheelScroll.x, event.mouseWheelScroll.y }, window, zoomAmount);
					std::cout << "Mouse x: " << event.mouseWheelScroll.x << ", \nMouse y:" << event.mouseWheelScroll.y << std::endl;
				}
			}

			else if (event.type == sf::Event::MouseButtonPressed)
			{
				sf::Vector2f mouse_world = window.mapPixelToCoords(sf::Mouse::getPosition(window));

				realPixel = tex.mapPixelToCoords({ sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y });
				std::cout << "Real X: " << (int)mouse_world.x << ", \n Real y:" << (int)mouse_world.y << std::endl;
				std::vector<DrawableNode>::iterator it =
					std::find_if(screenNodeVec.begin(), screenNodeVec.end(), [=](DrawableNode node) {return node.x == (int)mouse_world.x && node.y == (int)mouse_world.y; });
				int index = std::distance(screenNodeVec.begin(), it);

				if (index <= screenNodeVec.size() - 1 && index > 0)
				{
					std::cout << "BOYING: " << screenNodeVec[index].id << "," << screenNodeVec[index].x << ","  << screenNodeVec[index].y << std::endl;
					if (numberOfClicks == 1)
					{
						line[1].position = sf::Vector2f((int)mouse_world.x + 0.5, (int)mouse_world.y + 0.5);
						numberOfClicks++;
					}

					if (numberOfClicks == 0)
					{
						line[0].position = sf::Vector2f((int)mouse_world.x + 0.5, (int)mouse_world.y + 0.5);
						numberOfClicks++;
					}
				}
				else
				{
					std::cout << "INDEX OUT OF RANGE" << std::endl;
				}
			}
			


		}
		window.clear();	

		//for (const auto& node : newPoints)
			//window.draw(node.second);
			
		//window.draw(background);
		window.draw(points);
		if (numberOfClicks >= 2)
		{
			window.draw(line);

		}

		window.display();
	}
	
	std::cout << clock.getElapsedTime().asSeconds() << std::endl;
	std::cin.get();
	return 0;
}
